#!/bin/bash

DIR=`dirname $0`
cd $DIR
source ./config.sh

# installs everything on server

NAME=`ssh $SRV "source /etc/os-release && echo \\\$PRETTY_NAME"`
echo "System is $NAME"

if [ "$NAME" == "CentOS Linux 7 (Core)" ]; then
    scp ./src/setup_server_script.sh $SRV:
    ssh $SRV "sudo bash setup_server_script.sh"
    exit 0
fi

echo "I don't know how to install tools on $SRV server... You should do it manually: docker, docker-compose, fswatch, python2.7."
exit 1