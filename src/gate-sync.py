#!/usr/bin/env python
# -*- coding: utf-8 -*-

from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import sys
import os
import subprocess
import threading

# sync.py local ~/prj user@server:/home/user/prj 4901 4902
# sync.py remote /home/user/prj 4901 4902
# sync.py TYPE LOCALDIR user@server:REMOTEDIR LOCALPORT REMOTEPORT
script_path = sys.argv[0]
in_local_mode = False
PORT_NUMBER = -1
if sys.argv[1] == 'local':
    local_path = sys.argv[2]
    remote_path = sys.argv[3].split(':')[1]
    remote_server = sys.argv[3].split(':')[0]
    local_port = int(sys.argv[4])
    remote_port = int(sys.argv[5])
    PORT_NUMBER = local_port
    in_local_mode = True
elif sys.argv[1] == 'remote':
    remote_path = sys.argv[2]
    local_port = int(sys.argv[3])
    remote_port = int(sys.argv[4])
    PORT_NUMBER = remote_port
else:
    raise Exception('First argument is incorrect. Should be "local" or "remote".')


devnull = open(os.devnull, 'w')
if in_local_mode:
    #RSyncs local to remote with delete
    subprocess.Popen(['rsync', '-avz', '--exclude-from=./src/excludes.rsync', '--delete', local_path, remote_server + ':' + remote_path], stdout=devnull, stderr=subprocess.STDOUT)
    # Starts remotely: `sync.py remote /home/user/prj 4901 4902`
    remote_sync = remote_path + '/.development-gate/gate-sync.py'
    subprocess.Popen(['ssh', remote_server, 'cd ' + remote_path + '; mkdir .development-gate'], stdout=devnull, stderr=subprocess.STDOUT)
    subprocess.Popen(['scp', script_path, remote_server + ':' + remote_sync], stdout=devnull, stderr=subprocess.STDOUT)
    subprocess.Popen(['ssh', remote_server, 'cd ' + remote_path + '; nohup python ' + remote_sync + ' remote ' + remote_path + ' ' + local_port + ' ' + remote_port + ' & 2>&1'], stdout=devnull, stderr=subprocess.STDOUT)
    # Starts `fswatch` for `~/prj`
    subprocess.Popen(['sh', '-c', 'fswatch -o ' + local_path + ' --exclude=.git --exclude=.ds-copy | xargs -n1 -I{} curl -X GET http://localhost:' + local_port + '/fswatch &'], stdout=devnull, stderr=subprocess.STDOUT)
    # SAVES file list
    # dict: path -> last updated
    # 
    # 
    
    # Starts `tunnels.py` that runs needed tunnels and controlls their up state (`4902` and reverse `4901`).
else:
    # Starts `fswatch` for `/home/user/prj`
    subprocess.Popen(['sh', '-c', 'fswatch -o ' + remote_path + ' --exclude=.git --exclude=.ds-copy | xargs -n1 -I{} curl -X GET http://localhost:' + remote_port + '/fswatch &'], stdout=devnull, stderr=subprocess.STDOUT)
    # SAVES file list

    pass

def local_fs_watch():
    print('Some files changed locally')

def remote_fs_watch():
    print('Some files changed on remote :) ')

def remote_updated():
    if not in_local_mode:
        print('Error: impossible to process remote_updated event in remote mode \n')
        return
    print('remote updated')
    
def synced():
    print('synced')

class basicGetHandler(BaseHTTPRequestHandler):
	def do_GET(self):
            if self.path == '/fswatch':
                if in_local_mode:
                    local_fs_watch()
                else:
                    remote_fs_watch()
            elif self.path == '/remote-updated':
                remote_updated()
            elif self.path == '/synced':
                synced()
            else:
                self.send_response(404)
                self.send_header('Content-type','text/html')
                self.end_headers()
                self.wfile.write('Impossible URL')

            self.send_response(200)
            self.send_header('Content-type','text/html')
            self.end_headers()
            self.wfile.write('ok')
            return

try:
	server = HTTPServer(('', PORT_NUMBER), basicGetHandler)
	print('Started httpserver on port ' + str(PORT_NUMBER))
	server.serve_forever()
except KeyboardInterrupt:
	print('^C received, shutting down the web server')
	server.socket.close()


# def printit():
#   threading.Timer(5.0, printit).start()
#   print "Hello, World!"

# printit()