#!/usr/bin/env python
# -*- coding: utf-8 -*-

# usage:
#     tunnels.py reverse 4901 user@server:4901
#     tunnels.py normal 4902 user@server:4902
# creates tunnel and controls state
# to control connection we can use     
#     nc -dvzw10 localhost 4902 2>/dev/null || echo "not ok"
#     netstat -ln | grep [t]cp | grep :4902 | wc -l

