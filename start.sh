#!/bin/bash

DIR=`dirname $0`
cd $DIR
source ./config.sh

#1. It creates all needed directories.
echo "Checking directory..."
DIR_EXISTS=`ssh $SRV "test -d ${DEVPATH} && echo '1'"`
if [ -z "$DIR_EXISTS" ]; then 
    echo "Not created yet, creating..."
    ssh $SRV "sudo mkdir -p ${DEVPATH}; sudo chown ec2-user:ec2-user ${DEVPATH}"
fi

#2. Syncs data (wait until data be fully synced).
python src/gate-sync.py local $PROJECTS_PATH $SRV:$DEVPATH 4901 4902 &

#3. Starts main docker from docker-compose.yml file in project
#   (dev settings should be in ENV)
#4. Starts tunnels.sh




# Catch ctrl+c to stop containers and tunnels