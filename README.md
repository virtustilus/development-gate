# development-gate

Developers need powerful PC to develop their project and this project will help to work with small laptop only.
Another way is using remote development, but there are not much useful tools for that.
Just install this repository somewhere and configure what should start.

## Getting started:

### Preparing tools

You will need python, so install it with some package manager (homebrew in macosx for example).

### Sync project

Clone this repository. 
For example you have a project called "heroes", so you might choose name "heroesdev" for remote development. Then clone this repository into related folder name near to your project:

    git clone git@bitbucket.org:virtustilus/development-gate.git heroesdev

1) Prepare your code to return 'ok' string when you request /ping from your container on port 80.
2) Copy `config.sh.dist` to `config.sh` and change all parameters to yours.
3) Run `setup.sh` and it will install everything needed on CentOS 7 server (for MacOSX remote server you will need to manually install  Docker and docker-compose).

4) Now you can run `start.sh` when you want to start your dev environment.

It will create tunnel for 80 port from server and when you enable debugging in your IDE it will also create reverse tunnel for 9000 port.


# new Idea: 
## sync.py
local and remote modes
##### LOCAL:   

    sync.py local ~/prj user@server:/home/user/prj 4901 4902
    sync.py TYPE LOCALDIR user@server:REMOTEDIR LOCALPORT REMOTEPORT

1. RSyncs local to remote with delete
2. Starts remotely: `sync.py remote /home/user/prj 4901 4902`
0. Starts `fswatch` for `~/prj`
1. SAVES file list
2. Waits for new events on `4901`
3. Starts `tunnels.py` that runs needed tunnels and controlls their up state (`4902` and reverse `4901`).

processes 2 events:

0. "fswatch":
    
    - RSYNCs to remote copy
    - Notify remote with `synced` event

0. "remote-updated":

    - RSYNCs remote to copy
    - internal function to Sync files from Copy to Main

##### REMOTE

    sync.py remote /home/user/prj 4901 4902

0. Starts `fswatch` for `/home/user/prj`
0. SAVES file list
0. Waits for new events on `4902`

processes 2 events:

0. "fswatch":
    
    - Notify `synced` to port `4901`

0. "synced":

    - internal function to Sync files from Copy to Main

## tunnels.py

usage:

    tunnels.py reverse 4901 user@server:4901
    tunnels.py normal 4902 user@server:4902

creates tunnel and controls state

to control connection we can use 
    
    nc -dvzw10 localhost 4902 2>/dev/null || echo "not ok"
    netstat -ln | grep [t]cp | grep :4902 | wc -l



